1.1.1.
------

- add `--zip` argument to runmitos.py
- create overview png https://gitlab.com/Bernt/MITOS/-/merge_requests/45
- fix for start/stop prediction https://gitlab.com/Bernt/MITOS/-/merge_requests/42
- fix gff for circular features https://gitlab.com/Bernt/MITOS/-/merge_requests/50
- remove ete2 from requirements https://gitlab.com/Bernt/MITOS/-/merge_requests/44